<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('index-dashboard');

Route::get('/product', 'ProductsController@index')->name('index-product');
Route::get('/product/search', 'ProductsController@search')->name('search-product');
Route::get('/product/create', 'ProductsController@create')->name('create-product');
Route::post('/product/store', 'ProductsController@store')->name('store-product');
Route::get('/product/show/{slug}', 'ProductsController@show')->name('show-product');
Route::get('/product/edit/{id}', 'ProductsController@edit')->name('edit-product');
Route::get('/product/update/{id}', 'ProductsController@update')->name('update-product');
Route::get('/product/delete/{id}', 'ProductsController@destroy')->name('delete-product');


Route::get('/category', 'CategoryController@index')->name('index-category');
Route::get('/category/search', 'CategoryController@search')->name('search-category');
Route::get('/category/create', 'CategoryController@create')->name('create-category');
Route::post('/category/store', 'CategoryController@store')->name('store-category');
Route::get('/category/{slug}', 'CategoryController@show')->name('show-category');
Route::get('/category/edit/{id}', 'CategoryController@edit')->name('edit-category');
Route::get('/category/update/{id}', 'CategoryController@update')->name('update-category');
Route::get('/category/delete/{id}', 'CategoryController@destroy')->name('delete-category');


Route::get('/user', 'ProductsController@index')->name('index-user');
Route::get('/user/create', 'ProductsController@create')->name('create-user');
Route::get('/user/show/{slug}', 'ProductsController@show')->name('show-user');
Route::get('/user/edit/{id}', 'ProductsController@edit')->name('edit-user');
Route::get('/user/update/{id}', 'ProductsController@update')->name('update-user');
Route::get('/user/delete/{id}', 'ProductsController@destroy')->name('delete-user');


//PAGE LIST
Route::get('/contact', 'ProductsController@index')->name('contact');
Route::get('/blog', 'ProductsController@create')->name('blog');
