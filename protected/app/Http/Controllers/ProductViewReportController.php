<?php

namespace App\Http\Controllers;

use App\ProductViewReport;
use Illuminate\Http\Request;

class ProductViewReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductViewReport  $productViewReport
     * @return \Illuminate\Http\Response
     */
    public function show(ProductViewReport $productViewReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductViewReport  $productViewReport
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductViewReport $productViewReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductViewReport  $productViewReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductViewReport $productViewReport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductViewReport  $productViewReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductViewReport $productViewReport)
    {
        //
    }
}
