<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Cache;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newProductList=Product::whereNull('deleted_at')->take(8)->get();
        $headerCategories= Cache::rememberForever('header-sidebar', function () {
            return $category=Category::orderBy('order_at','ASC')->take(5)->get();
         });
        return view('welcome',['newProductList'=>$newProductList,'headerCategories'=>$headerCategories]);
    }
}
