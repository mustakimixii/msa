<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Category;
use Cache;

class FrontEndComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $headerCategories= Cache::rememberForever('header-sidebar', function () {
           return Category::take(5)->get();
        });
        $view->with('headerCategories', $headerCategories);
    }
}   
