<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            DB::table('categories')->insert([
                'name' => $faker->name,
                'slug' => str_slug($faker->name),
                'is_show_frontend' => 1,
                'order_at' => random_int(1, 10),
            ]);
        }
    }
}
