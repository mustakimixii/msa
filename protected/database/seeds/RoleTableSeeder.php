<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([[
            'id' => "1",
            'name' => "admin",
            'status' => "1"
        ],
        [
            'id' => "2",
            'name' => "customer",
            'status' => "1"
        ]]);
    }
}
