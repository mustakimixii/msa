<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => "admin",
            'username' => "admin",
            'role_id' => "1",
            'active' => "1",
            'blocked' => "1",
            'allow_news'=>"1",
            'is_confirmed'=>"1",
            'email' => 'admin@gg.com',
            'password' => bcrypt('admin'),
        ]);
    }
}
