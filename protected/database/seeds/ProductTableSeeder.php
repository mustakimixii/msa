<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 100) as $index) {
            DB::table('products')->insert([
                'name' => $faker->name,
                'user_id' => '1',
                'slug' => str_slug($faker->name),
                'price' => random_int(10, 100),
                'stock' => random_int(1, 1000),
                'featured_image_path'=>$faker->imageUrl($width = 640, $height = 480),
                'category_id' => random_int(1, 10),
                'description' => $faker->realText($maxNbChars = 200, $indexSize = 2)
            ]);
        }
    }
}
