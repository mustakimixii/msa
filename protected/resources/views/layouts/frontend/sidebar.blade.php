<!-- Sidebar -->

<div class="sidebar">

  <!-- Logo -->
  <div class="sidebar_logo">
    <a href="{{route('home')}}">
      <div>MSA</div>
    </a>
  </div>

  <!-- Sidebar Navigation -->
  <nav class="sidebar_nav">
    <ul>
      <li><a href="{{route('home')}}">home<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
      @foreach ($headerCategories as $item)
    <li><a href="{{route('show-category',[$item->slug])}}">{{$item->name}}<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>          
      @endforeach
    </ul>
  </nav>

  <!-- Search -->
  <div class="search">
    <form action="{{route('search-product')}}" class="search_form" id="sidebar_search_form">
      <input type="text" class="search_input" placeholder="Search" required="required" required>
      <button class="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
    </form>
  </div>

</div>