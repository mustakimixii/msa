@extends('layouts.admin.master')
@extends('layouts.admin.sidebar')
@section('content')
<!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <!-- FORM VALIDATION -->
    <div class="row mt">
      <div class="col-lg-12">

        @if (Session::has('message'))
        <div class="flash alert-info">
          <p class="panel-body">
            {{ Session::get('message') }}
          </p>
        </div>
        @endif
        <form class="cmxform form-horizontal style-form" id="commentForm" method="post"
          action="{{route('store-product')}}">

          {{csrf_field()}}
          <div class="form-group ">
            <h4><i class="fa fa-angle-right"></i> Form {{$title}}</h4>
            <div class="form-panel">
              <div class="form-group">
                <label class="control-label col-lg-2">Image Upload</label>
                <div class="col-md-9">
                  <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                      <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" alt="" />
                    </div>
                    <div class="fileupload-preview fileupload-exists thumbnail"
                      style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                    <div>
                      <span class="btn btn-theme02 btn-file">
                        <span class="fileupload-new"><i class="fa fa-paperclip"></i> Select image</span>
                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                        <input type="file" class="default" />
                      </span>
                      <a href="advanced_form_components.html#" class="btn btn-theme04 fileupload-exists"
                        data-dismiss="fileupload"><i class="fa fa-trash-o"></i> Remove</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group ">
                <label for="cname" class="control-label col-lg-2">Name (required)</label>
                <div class="col-lg-10">
                  <input class=" form-control" id="cname" name="name" minlength="2" type="text"
                    value="{{old('category')}}" required />
                </div>
              </div>
              <div class="form-group ">
                <label for="cname" class="control-label col-lg-2">Category (required)</label>
                <div class="col-lg-10">
                  {{-- <input class=" form-control" id="cname" name="category_id" minlength="1" type="text" required /> --}}
                  <select name="category_id" class="form-control" id="">
                      <option value="" selected disabled>-- Pilih Category -- </option>
                    @foreach ($categoryList as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group ">
                <label for="ccomment" class="control-label col-lg-2">Description (required)</label>
                <div class="col-lg-10">
                  <textarea class="form-control " id="description" name="description"
                    required>{{old('description')}}</textarea>
                </div>
              </div>
              <div class="form-group ">
                <label for="ccomment" class="control-label col-lg-2">Harga (required)</label>
                <div class="col-lg-10">
                  <input class=" form-control" id="cname" name="price" minlength="1" type="number"
                    value="{{old('price')}}" required />
                </div>
              </div>
              <div class="form-group ">
                <label for="ccomment" class="control-label col-lg-2">Stok (required)</label>
                <div class="col-lg-10">
                  <input class=" form-control" id="cname" name="stock" minlength="1" type="number"
                    value="{{old('stock')}}" required />
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                  <button class="btn btn-theme" type="submit">Save</button>
                  <button class="btn btn-theme04" type="button">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </form>
        <!-- /form-panel -->
      </div>
      <!-- /col-lg-12 -->
    </div>
    <!-- /row -->
    <!-- INLINE FORM ELELEMNTS -->
  </section>
  <!-- /wrapper -->
</section>
<!-- /MAIN CONTENT -->
<!--main content end-->
@endsection