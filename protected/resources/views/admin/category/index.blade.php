@extends('layouts.admin.master')
@extends('layouts.admin.sidebar')
@section('content')
<!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
          <!-- row -->
          <div class="row mt">
            <div class="col-md-12">
              <div class="content-panel">
                <table class="table table-striped table-advance table-hover">
                  <h4><i class="fa fa-angle-right"></i> Category List</h4>
                  <hr>
                  <thead>
                    <tr>
                      <th>No</th>
                      <th><i class="fa fa-bullhorn"></i> Name</th>
                      {{-- <th><i class="fa fa-image"></i> Image</th> --}}
                      {{-- <th class="hidden-phone" style="width:70%;"><i class="fa fa-question-circle"></i> Descrition</th> --}}
                      <th><i class="fa fa-bookmark"></i> Tampil Depan</th>
                      <th><i class=" fa fa-edit"></i> Urutan</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($category as $key =>  $item)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>
                        <a href="basic_table.html#">{{$item->name}}</a>
                      </td>
                      {{-- <td class="hidden-phone"><img src="{{$item->featured_image_path}}" width="300" height="240" alt="" srcset=""></td> --}}
                      {{-- <td class="hidden-phone" style="width:50%;">{{$item->description}}</td> --}}
                      <td>{{$item->is_show_frontend}} </td>
                      <td>{{$item->order_at}} </td>
                      <td>
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>

              <!-- /content-panel -->
            </div>
            <!-- /col-md-12 -->
          </div>
          <!-- /row -->
        </section>
      </section>
      <!-- /MAIN CONTENT -->
      <!--main content end-->
    @endsection