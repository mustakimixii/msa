@extends('layouts.master')
@section('content')
<!-- Product Catagories Area Start -->
<div class="products-catagories-area clearfix">
    <div class="amado-pro-catagory clearfix">


        @foreach ($newProductList as $item)

        <!-- Single Catagory -->
        <div class="single-products-catagory clearfix">
                <a href="shop.html">
                    <img src="{{$item->featured_image_path}}" alt="{{$item->name}}">
                    <!-- Hover Content -->
                    <div class="hover-content">
                        <div class="line"></div>
                        <p>Rp. {{$item->price}}</p>
                        <h4>{{$item->name}}</h4>
                    </div>
                </a>
            </div>
        @endforeach

        
    </div>
</div>
<!-- Product Catagories Area End -->
</div>
<!-- ##### Main Content Wrapper End ##### -->
@endsection